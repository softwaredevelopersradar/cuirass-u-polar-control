﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Random random = new Random();
        private double[] GenerateData()
        {
            int iCount = 360;
            double[] points = new double[iCount];
            for (int i = 0; i < iCount; i++)
            {
                points[i] = random.NextDouble() * 255;
            }
            return points;
        }

        private double[] GenerateData2()
        {
            int iCount = 360;
            double[] points = new double[iCount];
            for (int i = 0; i < iCount; i++)
            {
                points[i] = random.NextDouble() * 100 +125;
            }
            return points;
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            PolarControl.PlotCorrelationCurve2(GenerateData2());
        }
        private void two_Click(object sender, RoutedEventArgs e)
        {
            PolarControl.InitAngle(angle: random.NextDouble() * 360);
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            //PolarControl.InitAngle2(angle: random.NextDouble() * 360);
            PolarControl.InitAngle3(angle: random.NextDouble() * 360);
            PolarControl.
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            PolarControl.ClearData();
        }

        private void five_Click(object sender, RoutedEventArgs e)
        {
            PolarControl.ClearAngle();
        }

        private void six_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.AxisColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void seven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.LabelsColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void eight_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void nine_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.GraphBackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.DataColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));

        }

        private void eleven_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            PolarControl.VectorColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }
    }
}
