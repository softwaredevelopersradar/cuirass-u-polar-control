﻿using System;
using System.Linq;
using System.Windows.Media;
using System.Windows.Controls;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;

namespace PolarControl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class PControl : UserControl
    {
        public PControl()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);
            InitializeComponent();
        }

        private Color _DataColor;
        public Color DataColor
        {
            get { return _DataColor; }
            set
            {
                _DataColor = value;
                PLSP.LineStyle.Color = value;
            }
        }

        private Color _LabelsColor;
        public Color LabelsColor
        {
            get { return _LabelsColor; }
            set
            {
                _LabelsColor = value;
                polarAxis.MajorDivTickStyle.Color = value;
                polarAxis.GridAngular.LabelsColor = value;
            }
        }

        private Color _AxisColor;
        public Color AxisColor
        {
            get { return _AxisColor; }
            set
            {
                _AxisColor = value;
                polarAxis.AxisColor = value;
            }
        }

        private Color _BackGround;
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;
                polarChart.ChartBackground.Color = value;
            }
        }

        private Color _GraphBackGround;
        public Color GraphBackGround
        {
            get { return _GraphBackGround; }
            set
            {
                _GraphBackGround = value;
                polarChart.ViewPolar.GraphBackground.Color = value;
            }
        }

        private Color _VectorColor = Color.FromArgb(255,218,165,32); //#FFDAA520
        public Color VectorColor
        {
            get { return _VectorColor; }
            set
            {
                _VectorColor = value;
                if (polarChart.ViewPolar.Annotations.Count > 0)
                {
                    polarChart.ViewPolar.Annotations[0].ArrowLineStyle.Color = value;
                    polarChart.ViewPolar.Annotations[0].Fill.Color = value;
                    polarChart.ViewPolar.Annotations[0].Fill.GradientFill = GradientFill.Solid;
                }
            }            
        }

        public void PlotCorrelationCurve(double[] data)
        {
            int iCount = 360;
            PolarSeriesPoint[] points = new PolarSeriesPoint[iCount];
            for (int i = 0; i < iCount; i++)
            {
                try { points[i].Amplitude = data[i]; }
                catch { points[i].Amplitude = 0; }
                points[i].Angle = (double)i;
            }

            PLSP.Points = points;
        }

        public void PlotCorrelationCurve2(double[] data)
        {
            int iCount = data.Count() + 1;
            PolarSeriesPoint[] points = new PolarSeriesPoint[iCount];
            for (int i = 0; i < iCount - 1; i++)
            {
                try { points[i].Amplitude = data[i]; }
                catch { points[i].Amplitude = 0; }
                points[i].Angle = (double)i;
            }
            try { points[data.Count()].Amplitude = data[0]; }
            catch { points[data.Count()].Amplitude = 0; }
            points[data.Count()].Angle =0;

            PLSP.Points = points;
        }

        public void ClearData()
        {
            PLSP.Points = new PolarSeriesPoint[0];
        }

        public void InitAngle(double angle)
        {
            polarChart.ViewPolar.Annotations.Clear();

            AnnotationPolar vector = new AnnotationPolar(polarChart.ViewPolar, polarAxis);

            //vector.Style = AnnotationStyle.Arrow;

            vector.Style = AnnotationStyle.EllipseArrow;
            vector.Shadow.Visible = false; 

            vector.TextStyle.Visible = false;

            //Location is where the vector starts from
            vector.LocationCoordinateSystem = CoordinateSystem.AxisValues;
            vector.LocationAxisValues.Angle = 0;
            vector.LocationAxisValues.Amplitude = 0;
            //Target is where the vector points to. All vectors are equal length in this example. 
            vector.TargetAxisValues.Amplitude = polarAxis.MaxAmplitude;
            vector.TargetAxisValues.Angle = angle;
            //vector.ArrowLineStyle.Width = 3;
            //vector.ArrowEndAspectRatio = 1.5f;
            vector.ArrowLineStyle.Width = 8;
            vector.ArrowEndAspectRatio = 10f;
            vector.MouseInteraction = false;
            //vector.ArrowStyleBegin = ArrowStyle.Circle;
            //vector.ArrowStyleEnd = ArrowStyle.Arrow;
            //vector.ArrowLineStyle.Color = Colors.White;
            //vector.SizeScreenCoords.SetValues(50,25);
            //vector.SizeScreenCoords = new SizeFloatXY(10, 10);
            vector.TargetScreenCoords = new PointFloatXY(100, 100);

            vector.ArrowLineStyle.Color = _VectorColor;

            vector.Fill.Color = _VectorColor;
            vector.Fill.GradientFill = GradientFill.Solid;


            polarChart.ViewPolar.Annotations.Add(vector);
        }

        public void ClearAngle()
        {
            polarChart.ViewPolar.Annotations.Clear();
        }

        public void InitAngle3(double angle)
        {
            if (polarChart.ViewPolar.Annotations.Count == 0)
            {
                AnnotationPolar vector = new AnnotationPolar(polarChart.ViewPolar, polarAxis);

                vector.Style = AnnotationStyle.Arrow;

                vector.TextStyle.Visible = false;

                //Location is where the vector starts from
                vector.LocationCoordinateSystem = CoordinateSystem.AxisValues;
                vector.LocationAxisValues.Angle = 0;
                vector.LocationAxisValues.Amplitude = 0;
                //Target is where the vector points to. All vectors are equal length in this example. 
                vector.TargetAxisValues.Amplitude = polarAxis.MaxAmplitude;
                vector.TargetAxisValues.Angle = angle;
                vector.ArrowLineStyle.Width = 3;
                vector.ArrowEndAspectRatio = 1.5f;
                vector.MouseInteraction = false;
                vector.ArrowStyleBegin = ArrowStyle.Circle;
                vector.ArrowStyleEnd = ArrowStyle.Arrow;

                vector.ArrowLineStyle.Color = _VectorColor;
                vector.Fill.Color = _VectorColor;
                vector.Fill.GradientFill = GradientFill.Solid;

                polarChart.ViewPolar.Annotations.Add(vector);
            }
            else
            {
                polarChart.ViewPolar.Annotations[0].TargetAxisValues.Angle = angle;
            }
        }
    }
}
